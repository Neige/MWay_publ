from fit_model import *
import numpy as np
import matplotlib.pyplot as plt
import time


if __name__ == '__main__':
#----------------------------------------------------------------------
# real data
#----------------------------------------------------------------------

# Load the data [to do yourself, we want distance, height above the galactic plane, Lz, JR, age, [abudance/H]]

data = take_data_DR14(locids, age_source=1)
D_imp, z_imp, Lz_imp, Jr_imp, t_imp, fe, dlz, djr, R, ldata, bdata, locidata, apid = data

#----------------------------------------------------------
# parameters that generated the normalization samples
#----------------------------------------------------------
pm_prop = rm.all_parameters(srm=1.0347e3, tsfr=3, sigma_vr0=47.7, beta=0.311,
          rd=3.03, x=0.7, t_old=6, t_T=6, Rd_heating=21.)


#----------------------------------------------------------
# enrichment parameters and parameters that are to fit
#----------------------------------------------------------

# define the elements to fit (here 'Fe'), does not really matter for the purpose at hand
elements=['Fe']

# the enrichment parameters
pm_fe = rm.enrichment_parameters(gamma=0.15, Rs=8.5,
                                  grad=-0.07,Fm=-1)

# put together in a list (useful when there are several of them..)
pm_elements = [pm_fe]

# put all model parameters together (including radial migration etc.)
pm_target = rm.all_parameters(srm=3*220,
     elements=elements, pm_elements=pm_elements, c4=0.5, t1=0.11)


#==================================================================================
# Prepare the data
#==================================================================================
#-----------------------------------------------------------------------
# data for numerator and marginalization over uncertainties
#-----------------------------------------------------------------------
NN = 5000                                             # subsample 5000 stars from the dataset
indices = np.random.randint(0,len(D_imp)-1, NN)       


#------------------------------------------------------------------------
# Generate arrays of possible true values using a noise model
#------------------------------------------------------------------------

if error == True:
    N = 100

    # here, for each dimenstion and for each star, sample possible true values of 
    # the observables from the measurements:
    # sample from (Lz_true | Lz_obs, sigma_obs) or something like this,
    # the pdf p is often a Gaussian. Note for age, it's a Gaussian in log age...
    # I call it: q_obs(x_true | x_obs) below
    [Lz_true, Jr_true, t_true] = sample.draw_true_Lz_Jr_t(Lz_imp[indices],
               Jr_imp[indices], t_imp[indices],
               sLz, sJr, st, N, Lzm=0.1, LzM=1e5, Jm=1e-6, 
               JM=1e4, tm=0.1, tM=10, age_source='Ting')
 

    # no errors on distances here but make the distance array the right shape            
    D_true = D_imp[indices] + 0
    D_true.shape += (1,)
    D_true = D_true*np.ones((len(D_true), N))
    
    # same for height above the plane
    z_true = z_imp[indices] + 0
    z_true.shape += (1,)
    z_true = z_true*np.ones((len(z_true), N))

    # sample true metallicities for real data again according to some noise model
    fe_true = fe[indices] + 0
    fe_true.shape += (1,)
    fe_true = fe_true*np.ones((len(fe_true), N))    
    fe_true += np.random.randn(len(fe_true), N)*s_fe

    data_imp_red = [D_true, z_true, Lz_true, Jr_true, t_true, fe_true]
 
#------------------------------------------------------------------------
# precompute the model on parameter-independent stuff
#------------------------------------------------------------------------

if error == True:
    # Note: what is happening in this block is a more general way to sample
    # data points from a noise model. Here I just don't assume that
    # p(x_obs | x_true, sigma) = p(x_true | x_obs, sigma) which is what
    # people often assume with noise models (if it were e.g. a Gaussian).
    # But this is not true at least for the age uncertainties, since they
    # are gaussian only in log space.
    # this will end-up in the likelihood as a factor p_obs / q_obs
    # that is 1 when p_obs = q_obs.

    # create the 2d observed quantities to compute stuff
    Lz_imp_2d = Lz_imp[indices] + 0
    Lz_imp_2d.shape += (1,)
    Lz_imp_2d = Lz_imp_2d*np.ones((len(Lz_imp[indices]), N))
    
    Jr_imp_2d = Jr_imp[indices] + 0
    Jr_imp_2d.shape += (1,)
    Jr_imp_2d = Jr_imp_2d*np.ones((len(Jr_imp[indices]), N))
    
    t_imp_2d = t_imp[indices] + 0
    t_imp_2d.shape += (1,)
    t_imp_2d = t_imp_2d*np.ones((len(t_imp[indices]), N))
    
    # model that sampled the points in the survey volume
    #p_samples = data_model_R0(data_imp1_1, pm_prop, frc=frc, log=False)
    
    # q_obs(x_true | x_obs) that sampled the true values
    # calculate the sampling probability on the true values 
    q_Lz = sample.q_true_obs(Lz_true, Lz_imp_2d, sLz,  0.1, 1e5) 
    q_Jr = sample.q_true_obs(Jr_true, Jr_imp_2d, sJr, 1e-6, 1e4)
#    q_t = sample.q_true_obs(t_true, t_imp_2d, st, 0.1, 10)
    q_t = sample.q_true_t(t_true, t_imp_2d, st, 0.1, 10)
    q_dist = q_Lz*q_Jr*q_t
    
    # p_obs(x_obs, x_true) = the Gaussian errors evaluated on the observed data
    # given the true value
    p_obs_Lz = rm.gauss(Lz_imp_2d, Lz_true, s_Lz)
    p_obs_Jr = rm.gauss(Jr_imp_2d, Jr_true, s_Jr)
    p_obs_t = rm.gauss(np.log(t_imp_2d), np.log(t_true), s_t)/t_imp_2d
    p_obs = p_obs_Lz*p_obs_Jr*p_obs_t
    


#==============================================================================
# Optimize the likelihood
#==============================================================================
MLE = True

if MLE == True:

    # parameters to fit (replace with your own chemica enrichment parameters)
    pm_names = ['enrich_Fe.gamma', 'enrich_Fe.grad', 'enrich_Fe.Femax']  
    pm_guess = np.array([0.3, -0.07, 0.5])#4, 3, 0.7, 0.5])#.5, 0.2, -0.07, 0.5])

    # other parameters not useful here
    pm_target.Rd_heating = 17
    pm_target.c4 = 1
    pm_target.t_old = 6
    pm_target.t_T = 6
    pm_target.sigma_vr0 = 50

    # optimize the likelihood (replace 'broken' with the name of your own enrichment function)
    pm_best = minimize(minus_likelihood_obs, pm_guess, args=(pm_names, 
          data_imp_red, data_imp1_1, p_obs, q_dist, p_samples, pm_target, ['Fe'], True,'broken'),
          method='Nelder-Mead', options={'maxiter':6000, 'adaptive':True, 'xatol': 0.0001})

    print(pm_best)
    
    #tosave = np.zeros(len(pm_best.x)+1)
    #tosave[0] = NN
    #tosave[1:] = pm_best.x
    #t = Table.read('2019_06_17_fits_results_vary_N_data.fits')
    #t.add_row(tosave)
    #t.write('2019_06_17_fits_results_vary_N_data.fits', format='fits', overwrite=True)
    
    
#==============================================================================
# run the mcmc
#==============================================================================
run_mcmc = False

if run_mcmc == True:
    import emcee
    import corner
    from emcee.utils import MPIPool

    pool = MPIPool()
    if not pool.is_master():
        pool.wait()
        sys.exit(0)

    pm_names = ['enrich_Fe.gamma', 'enrich_Fe.grad']
    pm_min = np.array([0.01, -0.2])
    pm_max = np.array([1, -0.001])

    pm_mins = np.array([0.1, -0.07])
    pm_maxs = np.array([0.15, -0.06])

    n_dim = len(pm_names)
    n_walkers = 20
    n_cpu = 4
    n_iter = 5000
    n_burn = 5000
    
    # starting samples    
    start_guess = np.random.random((n_walkers,n_dim))
    start_guess = start_guess*(pm_maxs - pm_mins) + pm_mins
    
    sampler = emcee.EnsembleSampler(n_walkers, n_dim, posterior,
                 args = (pm_names, pm_min, pm_max, data_imp_red, 
                        data_imp1_1, pm_prop, pm_target, ['Fe']), pool=pool)
 
   
    # run the mcmc
    sampler.run_mcmc(start_guess, n_iter)
    #    print(p_pos(start_guess[0,:], pm_fit, data, arguments))
    
    # get the samples
    samples1 = sampler.chain
    
    # save
    np.save('mcmc_out/samples_pos2fields_2019_06_25_allapo.npy',samples1)

    pool.close()    

    corner.corner(samples1[:, n_burn:, :].reshape(-1, n_dim))
    plt.show()
    

    
    
    
