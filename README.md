# Milky Way Model with Radial Migration in Action

## Requirements
Python packages:

- `Galpy`
- `useful_functions` (local, has gitlab repo)
- `emcee`
- `numpy`, `astropy`, `matplotlib`, `scipy`

## Main features

This model accounts for an inside-out star formation history, gradual
enrichment of the gas in metals (for now just iron), vertical heating
of stars from Ting & Rix (2019) and paramatrizes the in-plane orbit
evolution as radial heating (blurring) and radial migration, or diffusion
in angula momentum.

It fully accounts for selection effects when combined with a selection 
function (see other package and the table in the github repo).

And uses MCMC or MLE to fit the best model parameters to APOGEE data.

The different codes are located in

- `rm_new.py` code used for paper 3 (or a very similar version of it in Aida desktop...)
- `fit_model.py` fitting APOGEE data to the model, used for paper 3
- `explore_sampling_parameters_betaversion.py` makes mock data (but the
version most up to date might be on alphawolf)
and `sample_data_kinematics_betaversion.py` goes with it.


## Most recent features

Recently started including an in-between proper parametric chemical
evolution model that is physically meaningful (or more than my 
weak chemical tagging). The new code versions that work together are

- `chem_waf_func.py` copy-pasted and adapted Weinberg's model (that he sent me by e-mail some time ago).
- `fit_model_Weinberg.py` that fits that similarly to the `fit_model.py` above.

## To do

- Finish coding the model to fit with Weinberg chemical enrichment...
- upgrade the versions of all the files...










