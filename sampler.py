# 2018-11-18
# 2018-12-12 correct bug in several fields: downsample
# 2019-08-21 add functions for apogee-2 (more general)
# 2021-01-24 add beta_srm
# 2021-08 Make usable to public

# packages
import numpy as np
from scipy import integrate
from astropy.table import Table, join, unique
import rm_new as rm
import useful_functions as uf
import cone
import random
from scipy.stats import truncnorm
from scipy import sparse
import useful_fcts as uf
import rm_new as rm
import pickle
import random


random.seed(0)


# parameters for generating 
class sampling_parameters(object):
    def __init__(self, rd=None, hz=None, srm=None,
         tsfr=None, sigma_vr0=None, t1=None, t_T=None,
         beta=None, c4=None, Rd_heating=None,x=None,
         t8=None, beta_srm=None):
        """
        Initialize the parameters to data
        """
        if rd is not None:
            self.rd = rd
        else:
            self.rd = 4.
        if hz is not None:
            self.hz = hz
        else:
            self.hz = 0.3
        if srm is not None:
            self.srm = srm
        else:
            self.srm = 4.5
        if beta_srm is not None: self.beta_srm = beta_srm
        else: self.beta_srm = 0.5
        if tsfr is not None:
            self.tsfr = tsfr
        else:
            self.tsfr = 8.
        if sigma_vr0 is not None:
            self.sigma_vr0 = sigma_vr0
        else:
            self.sigma_vr0 = 40.
        if t1 is not None:
            self.t1 = t1
        else:
            self.t1 = 0.11
        if t_T is not None:
            self.t_T = t_T
        else:
            self.t_T = 8.
        if beta is not None:
            self.beta = beta
        else:
            self.beta = 0.33
        if c4 is not None:
            self.c4 = c4
        else:
            self.c4 = 0.5
        if Rd_heating is not None:
            self.Rd_heating = Rd_heating
        else: 
            self.Rd_heating = 8
        self.R8 = 8
        if x is not None: self.x =x
        else: self.x = 0.7
        if t8 is not None: self.t8 = t8
        else: self.t8 = 10

# cumulative function
def f_cumul(x1, y1, axes=0):
    """
    cumulative of y1
    x1 needs to be linspace
    """
    z = np.cumsum(y1, axis=axes)*(x1[1]-x1[0])
    return z

def rho(R, z, p):
    f = np.exp(-R/p.rd)*np.exp(-np.abs(z)/p.hz)/(2.*p.hz)/p.rd/p.rd
    return f

def age(n, pm):
    """

    stellar ages 
    according to the RC distribution 
    and exponential SFH

    Arguments
    n = number of points to sampel from
    pm = model parameters for SFH
         (according to model file)


    Return
    array of ages [Gyr]
    """

    # normalization constant
    c1 = rm.norm_c1(rm.tau_min, rm.tau_max, pm)

    # compute age distribution on points
    t_lin = np.linspace(rm.tau_min, rm.tau_max, 1000000)
    y_lin = rm.p_age(t_lin, c1, pm)

    # calculate the cumulative 
    # and interpolate the inverse
    u = np.random.rand(n)
    tau = np.interp(u, f_cumul(t_lin, y_lin), t_lin)
    return tau

def invert_age(u,  R, tsfr2, x, t8, tau_max, tmin):
    """
    2020-10-27
    Invert the cumulative of the SFH (exactly)
    u = F(t) = integral_tmin^t SFH_R_1(t, R0)
    """
    a = x

    #print(tsfr2, R, alpha)
    l = 1/tsfr2*R/8*a

    c = (np.exp(-t8*(l - 1/tsfr2) - tau_max/tsfr2)\
                   -np.exp(-tmin*(l - 1/tsfr2) - tau_max/tsfr2))

    y = -1/(l-1/tsfr2)*(np.log(c*u + np.exp(-tmin*(l - 1/tsfr2) - tau_max/tsfr2)) + tau_max/tsfr2)
    return y

def age_IO(R0, pm, tmin=0.1, frc_t=0, tau_max=12):
    """
    2020-10-27
    Sample exactly from the star formation history
    """
    u = np.random.rand(len(R0))
    y = invert_age(u,  R0, pm.tsfr, pm.x, pm.t8, tau_max, tmin)
    return y

def age_IO_old(R0, pm, tmin=0.1, frc_t=0):
    """
    2019-06-19
    2019-06-23 add frc_t
    Used in frankel+20

    stellar ages in function of birth radius
    1. Find R0_flat
    2. R0 < R0_flat - 1.5 --> decreasing expo
    3. R0 > R0_flat + 1.5 --> increasing expo
    4. rest: uniform

    Arguments
        R0 = Galactocentric radius [kpc]
        pm = sampling parameters
        tmin = minimum age to which the model is normalized
        frc_t = 0 or number to add to the time-scale for the inner disk
              if frc > 0: will select RC star later
              so here already penalize old stars in the inner disk
              by elongating the star formation time-scale
    """
    R0_flat = 8/pm.x
    m_in = (R0 < R0_flat - 1.5)
    m_out = (R0 > R0_flat + 1.5)
    m_middle = (R0 > R0_flat - 1.5) & (R0 < R0_flat + 1.5)
    tsfr = pm.tsfr
    t = np.zeros(len(R0))
    n_in = np.sum(m_in)
    n_out = np.sum(m_out)
    n_middle = np.sum(m_middle)
    t_in = np.random.exponential(scale=tsfr+frc_t, size=n_in)
    t_in1 = pm.t8 - t_in
    mneg = t_in1 < 0
    nneg = np.sum(mneg) 
    while nneg > 0:
        tt = np.random.exponential(scale=tsfr, size=nneg)
        tt1 = pm.t8 - tt
        t_in1[mneg] = tt1
        mneg = t_in1 < 0
        nneg = np.sum(mneg)
    t[m_in] = t_in1 + 0

    t_out = np.random.exponential(scale=tsfr, size=n_out)
    mt = t_out > pm.t8
    nt = np.sum(mt)
    while nt > 0:
        tt = np.random.exponential(scale=tsfr, size=nt)
        t_out[mt] = tt
        mt = t_out > pm.t8
        nt = np.sum(mt)

    t[m_out] = tmin + t_out
    t[m_middle] = np.random.rand(n_middle)*(pm.t8 - tmin) + tmin
    return t

def p_age_IO(t, R0, pm, tmin=0.1, frc_t=0):
    """
    2019-06-19
    2019-06-23 add frc_t

    Probability associated with the sampling in the
    age_IO() function

    Arguments
        t ndarray, age [Gyr]
        R0 ndarray, Galactocentric radius [kpc]
        pm = sampling parameters object
        tmin = minimum age
        frc_t = 0 --> no RC selection
                x Gyr --> elongate SFR time-scale in the inner disk
                          to penalize old stars (RC selection later)
    """
    R0_flat = 8/pm.x
    m_in = (R0 < R0_flat - 1.5)
    m_out = (R0 > R0_flat + 1.5)
    m_middle = (R0 > R0_flat - 1.5) & (R0 < R0_flat + 1.5)
    tsfr = pm.tsfr
    c_out = -(np.exp(-pm.t8/tsfr) - np.exp(-tmin/tsfr))*tsfr
    c_in = (1 - np.exp(-(pm.t8 - tmin)/(tsfr+frc_t)))*(tsfr + frc_t)
    c_middle = (pm.t8 - tmin)
    p = np.zeros(len(t))
    p[m_in] = np.exp(-(pm.t8 - t[m_in])/(tsfr + frc_t))/c_in
    p[m_out] = np.exp(-t[m_out]/tsfr)/c_out
    p[m_middle] = 1/c_middle
    return p

def produce_R(pm, n):
        """
        Sample birth radii

        2021-01-24
        frankel
        """
        r0_lin = np.linspace(0, 60, 1000000)
        y_lin = rm.p_birth(r0_lin, pm)*r0_lin/pm.rd

        u = np.random.rand(n)
        R0 = np.interp(u, f_cumul(r0_lin, y_lin), r0_lin)
        return R0

def fraction_area(x, lid, map_version=17):
    """
    fractional area of a field where RC stars can be visible

    arguments:
    x = distance where to compute 
        the fractional area (kpc): float or ndarray
    lid =  location id of the field where to compute the fraction
    map_version = 17 or 19 (default: 17) for bayestar17

    returns::
    an interpolation of the pre-computed fractional area
    (stored in folder 'fractions_areas_RC'
    foat or ndarray of length len(x)

    2019-06-24 add map version choice
    """
    # choose the map version
    if map_version == 17:
        folder_name='fractions_area_allfields/'
    elif map_version == 19:
        'fractions_area_allfields_2019/'

    # load the visibility fractions
    d_dust, area_dust = np.load(folder_name\
                        +str(int(lid))+'_indebetouw.npy')

    # interpolate for the distance values x
    f = np.interp(x, d_dust, area_dust)
    return f


def draw_single_pointing_expo(n, l_p, b_p, D_minp, D_maxp, 
               pm, radius_plate, lid, density=rho, dust=False):
    """
    Draws positions R, z, l, b, D in single pointings

    arguments
    n = initial number of points to from
    l_p = longitude pointing (deg)
    b_p = latitude pointing (deg)
    Dmin_p, D_maxp = min and max distances in pointing
    Rd = scale length (approximate)
    hz = scale height (approximate)
    """
    # interpolate the cumulative of the distribution
    n_interp = 1000
    D_linear = np.linspace(D_minp, D_maxp, n_interp)

    # Galactocentric dist & height along the pointing
    R_along = uf.R_lbd(l_p, b_p, D_linear)
    z_along = uf.z_bd(b_p, D_linear)

    # distribution evaluation
#    p = rho(R_along, z_along, pm)\
#        *D_linear*D_linear
    p = density(R_along, z_along, pm)*D_linear*D_linear

    # dust
    if dust == True:
        p *= fraction_area(D_linear, lid) 

    # cumulative distribution:
    F_cumul = f_cumul(D_linear, p)

    u = np.random.rand(n)*np.max(F_cumul)
    D = np.interp(u, F_cumul, D_linear)

    l_s, b_s, D = cone.make_cone_lbd(D, l_p, b_p, radius_plate)

    R, z = uf.convert_lbd_Rgalz(l_s, b_s, D)
    return R, z, l_s, b_s, D, np.max(F_cumul)



def draw_single_pointing_expo_DR(n, l_p, b_p, D_linear, frac_dust,
                         pm, radius_plate, density=rho, dust=True):


    """
    Draws positions R, z, l, b, D in single pointings
    Variant more general from the version above, takes
    in different arguments (i.e. directly dust instead
    of querying for it all the time).

    arguments
    n = initial number of points to from
    l_p = longitude pointing (deg)
    b_p = latitude pointing (deg)
    D_linear: distance array
    frac_dust: fraction not extinguished by dust (same size as distance)
    pm = sampling parameters
    radius_plate = radius of the plate to from
    dust: boolean: whether to account for it or not

    2019-08-21
    """

    # Galactocentric dist & height along the pointing
    R_along, z_along = uf.convert_lbd_Rgalz(l_p, b_p, D_linear)

    # distribution evaluation
    p = density(R_along, z_along, pm)*D_linear*D_linear

    # dust
    if dust == True:
        p *= frac_dust

    # cumulative distribution:
    F_cumul = f_cumul(D_linear, p)

    u = np.random.rand(n)*np.max(F_cumul)
    D = np.interp(u, F_cumul, D_linear)

    l_s, b_s, D = cone.make_cone_lbd(D, l_p, b_p, radius_plate)
    R, z = uf.convert_lbd_Rgalz(l_s, b_s, D)
    return R, z, l_s, b_s, D, np.max(F_cumul)


def draw_single_pointing_expo_D(n, l_p, b_p, D_minp, D_maxp,
               pm, radius_plate, lid, density=rho, dust=False):
    """
    2019-01-10

    Draws positions R, z, l, b, D in single pointings
    do not distribute the stars intside the fields
    (only distances)

    arguments
    n = initial number of points to from
    l_p = longitude pointing (deg)
    b_p = latitude pointing (deg)
    Dmin_p, D_maxp = min and max distances in pointing
    Rd = scale length (approximate)
    hz = scale height (approximate)

    """
    # interpolate the cumulative of the distribution
    n_interp = 1000
    D_linear = np.linspace(D_minp, D_maxp, n_interp)

    # Galactocentric dist & height along the pointing
    R_along, z_along = uf.convert_lbd_Rgalz(l_p, b_p, D_linear)
    
    # distribution evaluation
    p = density(R_along, z_along, pm)*D_linear*D_linear

    # dust
    if dust == True:
        p *= fraction_area(D_linear, lid)

    # cumulative distribution:
    F_cumul = f_cumul(D_linear, p)

    u = np.random.rand(n)*np.max(F_cumul)
    D = np.interp(u, F_cumul, D_linear)

    l_s, b_s = l_p*np.ones(len(D)), b_p*np.ones(len(D))
    R = uf.R_lbd(l_s, b_s, D)
    z = uf.z_bd(b_s, D)
    return R, z, l_s, b_s, D, np.max(F_cumul)


def draw_several_pointings_expo(n, l_p, b_p, D_minp, D_maxp,
                    pm, radius_plate, area, selfrac, locids, 
                 density=None, dust=False, return_maxF=False,
                                        return_Nkeep=False):

    """
    Draw data in each pointing provided
 
    arguments
       n = number of  in the highest fraction field
       l_p = list of longitudes of the fields
       b_p = list of latitudes of the fields
       D_minp, D_maxp = lists of min and max distances in thr fields
       pm parameter object:
           pm.Rd = scale length of the disk
           pm.hz = scale height of the disk
       radius_plate = list of radii for the plates of the fields
       area = area of each field
       selfrac = list of selection fractions for each fields 
                 will be rescaled on the most selected field to 1
       locids = location ids of the fields such that we know to
             which field each data point belongs
       density = model for the disk density (default = rho double expo)
       dust = boolean: account for dust or not
       return_maxF = boolean: do you want the normalization constant
       return_Nkeep = boolean: do you want the nb of points per field


    return
       R = array of galactocentric radii  [kpc]
       z = array of heights above the plane [kpc]
       l = array of longitudes [deg]
       b = array of latitudes [deg]
       D = array of distances to the Sun [kpc]
       loc = list of locations of each field
       maxF = list normalization constant for each field 
              (including selfrac & area)
       Nkeep = array (same size as R) 
               number of points generated in field
    """
    # density
    if density is None:
        density=rho

    # make empty lists for the data
    R, z, l, b, D, locs = ([] for i in range(6))

    # max of the f cumul to downsample
    maxF = np.zeros(len(l_p))

    # selection fractions to downtoo
#    frac_max = np.max(selfrac)
#    selfrac_scaled = selfrac/frac_max

    # loop over single pointnigs and make mock
    for i in range(len(l_p)):
        Ri, zi, li, bi, Di, mF = draw_single_pointing_expo(n, l_p[i], b_p[i], 
                                   D_minp[i], D_maxp[i], pm, radius_plate[i], 
                                               locids[i], density, dust=dust)

        R.append(Ri)
        z.append(zi)
        l.append(li)
        b.append(bi)
        D.append(Di)
        maxF[i] = mF

    # find the max F to down and scale also selfrac and area
    # Added 12/12/2018 
    max_Fcumul = np.max(maxF*np.array(selfrac)*np.array(area))
    F_cumul_scaled = np.array(selfrac)*maxF*np.array(area)/max_Fcumul

    # make empty arrays for the data
    R_arr = np.array([])
    z_arr = np.array([])
    l_arr = np.array([])
    b_arr = np.array([])
    D_arr = np.array([])
    locs = np.array([])
    Nkeep = np.array([])

    # now downeach according to the selection fraction and field size
    random.seed(1)  # set random seed for smoothness
    for i in range(len(l_p)):
        n_keep = int(n*F_cumul_scaled[i])
        indices = random.range(n, n_keep)

        # concatenate
        R_arr = sparce.crs_matrix(np.concatenate((R_arr, R[i][indices]))).data
        z_arr = sparce.crs_matrix(np.concatenate((z_arr, z[i][indices]))).data
        l_arr = sparce.crs_matrix(np.concatenate((l_arr, l[i][indices]))).data
        b_arr = sparce.crs_matrix(np.concatenate((b_arr, b[i][indices]))).data
        D_arr = sparce.crs_matrix(np.concatenate((D_arr, D[i][indices]))).data
        locs = np.concatenate((locs, locids[i]*np.ones(len(indices))))
        Nkeep = np.concatenate((Nkeep, n_keep*np.ones(n_keep)))

    # return the data
    if return_maxF == False:
        return R_arr, z_arr, l_arr, b_arr, D_arr, locs
    else:
        if return_Nkeep == False:
            return R_arr, z_arr, l_arr, b_arr, D_arr, locs, maxF

        else:
            return R_arr, z_arr, l_arr, b_arr, D_arr, locs, maxF, Nkeep





def draw_several_pointings_expo_DR(n, l_p, b_p, D_linear, frac_dust,
               pm, radius_plate, area, selfrac, locids, density=None, 
                   dust=True, return_maxF=False, return_Nkeep=False):

    """
    Draw data in each pointing provided
 
    arguments
       n = number of  in the highest fraction field
       l_p = list of longitudes of the fields
       b_p = list of latitudes of the fields
       D_linear = distances (array)
       pm parameter object:
           pm.Rd = scale length of the disk
           pm.hz = scale height of the disk
       radius_plate = list of radii for the plates of the fields
       area = area of each field
       selfrac = list of selection fractions for each fields 
                 will be rescaled on the most selected field to 1
       locids = location ids of the fields such that we know to
             which field each data point belongs
       density = model for the disk density (default = rho double expo)
       dust = boolean: account for dust or not
       return_maxF = boolean: do you want the normalization constant
       return_Nkeep = boolean: do you want the nb of points per field


    return
       R = array of galactocentric radii  [kpc]
       z = array of heights above the plane [kpc]
       l = array of longitudes [deg]
       b = array of latitudes [deg]
       D = array of distances to the Sun [kpc]
       loc = list of locations of each field
       maxF = list normalization constant for each field 
              (including selfrac & area)
       Nkeep = array (same size as R) 
               number of points generated in field

    2019-08-21
    """
    # density
    if density is None:
        density=rho

    # make empty lists for the data
    R, z, l, b, D, locs = ([] for i in range(6))

    # max of the f cumul to downsample
    maxF = np.zeros(len(l_p))

    # loop over single pointnigs and make mock
    for i in range(len(l_p)):
        Ri, zi, li, bi, Di, mF = draw_single_pointing_expo_DR(n, l_p[i], b_p[i],
             D_linear[i], frac_dust[i], pm, radius_plate[i], density, dust=dust)

        R.append(Ri)
        z.append(zi)
        l.append(li)
        b.append(bi)
        D.append(Di)
        maxF[i] = mF

    # find the max F to down and scale also selfrac and area
    # Added 12/12/2018 
    max_Fcumul = np.max(maxF*np.array(selfrac)*np.array(area))
    F_cumul_scaled = np.array(selfrac)*maxF*np.array(area)/max_Fcumul

    # make empty arrays for the data
    R_arr = np.array([])
    z_arr = np.array([])
    l_arr = np.array([])
    b_arr = np.array([])
    D_arr = np.array([])
    locs = np.array([])
    weight = np.array([])
    Nkeep = np.array([])

    # now downeach according to the selection fraction and field size
    random.seed(1) # set random seed for smoothness
    for i in range(len(l_p)):
        n_keep = int(n*F_cumul_scaled[i])
        indices = random.sample(range(n), n_keep)

        # concatenate
        R_arr = np.concatenate((R_arr, R[i][indices]))
        z_arr = np.concatenate((z_arr, z[i][indices]))
        l_arr = np.concatenate((l_arr, l[i][indices]))
        b_arr = np.concatenate((b_arr, b[i][indices]))
        D_arr = np.concatenate((D_arr, D[i][indices]))
        locs = np.concatenate((locs, locids[i]*np.ones(len(indices))))
        weight = np.concatenate((weight, selfrac[i]*np.ones(len(indices))))
        Nkeep = np.concatenate((Nkeep, n_keep*np.ones(n_keep)))

    # return the data
    if return_maxF == False:
        return R_arr, z_arr, l_arr, b_arr, D_arr, locs, weight
    else:
        if return_Nkeep == False:
            return R_arr, z_arr, l_arr, b_arr, D_arr, locs, maxF

        else:
            return R_arr, z_arr, l_arr, b_arr, D_arr, locs, maxF, Nkeep


def draw_several_pointings_expo_D_noreject(n, l_p, b_p, D_minp, D_maxp,
                               pm, radius_plate, area, selfrac, locids,
                           density=rho, dust=False, return_maxF=False):

    """
    2019-01-10

    Draw data in each pointing provided, does not distribute stars
    inside the fields, and do not reject points to reweight the
    number of stars in each field
 
    arguments
    n = number of  in the highest fraction field
    l_p = list of longitudes of the fields
    b_p = list of latitudes of the fields
    D_minp, D_maxp = lists of min and max distances in thr fields
    pm parameter object:
        pm.Rd = scale length of the disk
        pm.hz = scale height of the disk
    radius_plate = list of radii for the plates of the fields
    area = area of each field
    selfrac = list of selection fractions for each fields 
              will be rescaled on the most selected field to 1
    locids = location ids of the fields such that we know to
             which field each data point belongs

    return
    R = array of galactocentric radii  [kpc]
    z = array of heights above the plane [kpc]
    l = array of longitudes [deg]
    b = array of latitudes [deg]
    D = array of distances to the Sun [kpc]
    """

    # make empty lists for the data
    R, z, l, b, D, locs = ([] for i in range(6))

    # max of the f cumul to downsample
    maxF = np.zeros(len(l_p))

    # loop over single pointnigs and make mock
    for i in range(len(l_p)):
        # mock data in single pointing
        Ri, zi, li, bi, Di, mFi = draw_single_pointing_expo_D(n, l_p[i], b_p[i],
                                     D_minp[i], D_maxp[i], pm, radius_plate[i],
                                                 locids[i], density, dust=dust)

        # append the list of physical quantities for each pointing
        R.append(Ri)
        z.append(zi)
        l.append(li)
        b.append(bi)
        D.append(Di)
        maxF[i] = mFi

    # make empty arrays for the data
    R_arr = np.concatenate(R)
    z_arr = np.concatenate(z)
    l_arr = np.concatenate(l)
    b_arr = np.concatenate(b)
    D_arr = np.concatenate(D)
    area_arr = np.array([])
    selfrac_arr = np.array([])
    locs = np.array([])
    maxf = np.array([])

    # now downeach
    for i in range(len(l_p)):
        locs = np.concatenate((locs, locids[i]*np.ones(n)))
        area_arr = np.concatenate((area_arr, area[i]*np.ones(n)))
        selfrac_arr = np.concatenate((selfrac_arr, selfrac[i]*np.ones(n)))
        maxf = np.concatenate((maxf, maxF[i]*np.ones(n)))

    # return the data
    if return_maxF == False:
        return R_arr, z_arr, l_arr, b_arr, D_arr, locs
    else:
        return R_arr, z_arr, l_arr, b_arr, D_arr, locs, maxf, area_arr, selfrac_arr



#---------------------------------------------------------------
# to check
#---------------------------------------------------------------
def p_vphi_Schoenrich(vphi, R, vel_disp, pm):
    """
    vphi distribution from Schoenrich 2012
    treats well asymmetric drift

    Arguments:
    vphi = azimyuthal velocity
    R = Galactocentric radius
    vel_disp = velocity dispersion at R and t
    pm = sampling parameters object

    Return:
    p(vphi | R, t) not normalized
    """
#    vc = 220
#    Rc = R*vphi/vc
    Rc = rm.R_circ(R*vphi)
    print(Rc)
    vc = vphi*R/Rc
    veldisp = rm.vel_disp_r(Rc, 4, pm)
    c = vc**2/(2*veldisp*veldisp)
    print(c)
    gc = np.sqrt(np.pi/(2*(c - 0.913)))
    y = np.exp(c*(2*np.log(vphi/vc) + 1 - vphi*vphi/(vc*vc)) \
                                          - (Rc - pm.R8)/pm.rd)
    return y/gc

def normed_p_vphi_Schoenrich(vphi, R, vel_disp, pm):
    const = integrate.quad(lambda vphi1:
         p_vphi_Schoenrich(vphi1, R, vel_disp, pm), 0.1, 1000)[0]
    return p_vphi_Schoenrich(vphi, R, vel_disp, pm)/const

def v_a_Schoenrich(R, vel_disp,pm):
    v_a = integrate.quad(lambda vphi: vphi*
        normed_p_vphi_Schoenrich(vphi, R, vel_disp, pm), 0.1, 1000)[0]
    return v_a

def p_vphi_approx(vphi, R, t, pm):
    """
    proba of vphi when negecting
    the asymmetric drfit and take
    the circular velocity centered on R
    """
    vcirc = rm.calculate_vcirc(R)
    veldisp = rm.vel_disp_r(R, t, pm)
    return rm.gauss(vphi, vcirc, veldisp)


def p_vphi_ad(vphi, R, t, pm):
    """
    proba of vphi when accounting for
    asymmetric drift as in Ting+13
    """
    veldisp = rm.vel_disp_r(R, t, pm)
    vcirc = rm.calculate_vcirc(R)
    A = (1./2.5 + 1./3.5)*R - 0.5
    vdrift = A*veldisp**2/(2*vcirc)
    p = rm.gauss(vphi, vcirc - vdrift, veldisp)
    return p, vdrift


def draw_velocities(R, z, t, pm):
    """
    draws velocities in R and phi directions from
    given positions

    arguments:
    R = Galacocentric radius [kpc]
    z = height above the plane [kpc]
    t = stellar age [kpc]

    returns
    vphi = azimuthal velocity [km/s]
    vr = velocity in the Galactocentric radius direction [km/s]
    """
    vel_disp = rm.vel_disp_r(R, t, pm)

    # accentuate velocity dispersion to envelope the distribution
    vel_disp *= 1.01

    # vphi around the circular velocity at R
    vcirc = rm.calculate_vcirc(R)

    # vphi (Schoenrich 2012)
#    A = (1./3.5 + 1./4.)*R - 0.5
#    vdrift = A*vel_disp**2/(2*vcirc)
    vphi = np.random.randn(len(R))*vel_disp + vcirc #- vdrift

    # vR around zero
    vR = np.random.randn(len(R))*vel_disp

    return vphi, vR

def draw_R0(R, t, pm):
    """
    Draw birth radii from current radii and age

    Approximate radial migration effects

    Arguments
    R = present day Galactocentric radius [kpc]
    t = age [Gyr]

    Returns
    R0 = birth Galactocentric radius [kpc]

    frankel
    2021-01-24 add beta_srm power law exponent for migration time dep
    """
    sigma = pm.srm*(t/12)**pm.beta_srm
    D = -pm.c4*pm.srm**2/(2*pm.rd*12)
    R0 = R + D*t + np.random.randn(len(R))*sigma
    return R0


def draw_true_Lz_Jr_t(Lzobs, Jrobs, tobs,
                      s_Lz, s_Jr, s_t, N,
                      Lzm=0, LzM=10000, Jm=0, 
                      JM=1000, tm=0, tM=12, age_source='Ness19'):
    """
    2019-06-24

    points according to a distribution
    q(Lz, Jr, t | Lzobs, Jrobs, tobs) that's a Gaussian
    centered on the observed value and of given std

    Arguments:
        Lzobs = ndarray of Lz obs [kpc km/s]
        Jrobs = ndarray of Jr obs [kpc km/s]
        tobs = ndarray of age [Gyr]
        s_Lz = ndarray std in Lz of size of Lzobs
        s_Jr = ndarray std in Jr of size of Jrobs
        s_t = ndarray std in t of sizr tobs
        N = number of  to generate

    Returns:
        [Lz, Jr, t]  from this (list), where
        Lz is ndarray of size (len(Lzobs), N)
        Jr  '' '  '
        t   ' ' ''' 

    TO DO: make such that the limits 
           are taken as arguments
    """

    # define the limits
    Lzmin = np.zeros(len(Lzobs))+Lzm
    Lzmax = np.ones(len(Lzobs))*LzM
    Jrmin = np.zeros(len(Jrobs))+Jm
    Jrmax = np.ones(len(Jrobs))*JM
    tmin = np.zeros(len(tobs))+tm
    tmax = np.ones(len(tobs))*tM
    
    # translate to scipy scale
    aLz, bLz = (Lzmin - Lzobs)/s_Lz, (Lzmax - Lzobs)/s_Lz
    aJr, bJr = (Jrmin - Jrobs)/s_Jr, (Jrmax - Jrobs)/s_Jr
    at, bt = (tmin - tobs)/s_t, (tmax - tobs)/s_t

    # the points
    Lz = truncnorm.rvs(aLz, bLz, loc=Lzobs, scale=s_Lz, size = (N, len(Lzobs))).T
    Jr = truncnorm.rvs(aJr, bJr, loc=Jrobs, scale=s_Jr, size = (N, len(Jrobs))).T
    t = truncnorm.rvs(at, bt, loc=tobs, scale=s_t, size = (N, len(tobs))).T

    if age_source == 'Ting':
        at, bt = (np.log(tmin) - np.log(tobs))/s_t, (np.log(tmax) - np.log(tobs))/s_t

        a = truncnorm.rvs(at, bt, loc=np.log(tobs),\
                             scale=s_t, size = (N, len(tobs))).T
        t = np.exp(a)

    return [Lz, Jr, t]


def q_true_obs(xtrue, xobs, s, xm, xM):
    """
    2019-06-24

    calculate the probability associated with
    draw_true_Lz_Jr_t()
    truncated Gaussian

    Arguments:
        xtrue -- random variable
        xobs -- mean
        s -- std
        xm -- minimum truncation
        xM -- maximum truncaction

    Returns:
        pdf(xtrue | xobs)

    """
    c = rm.integrate_gauss(xobs, xm, xM, s)
    y = rm.gauss(xtrue, xobs, s)/c
    return y

def q_true_t(ttrue, tobs, s, tm, tM):
    c = rm.integrate_gauss_log(tobs, tm, tM, s)
    y = rm.gauss(np.log(ttrue), np.log(tobs), s)/ttrue/c
    return y


#################################################################################
# Function that  points given
# -- sampling parameters
# -- model parameters
# -- number of points
#################################################################################

def points_in_given_fields(pm_sample, pm_model, n, fieldstuff, dust=False,
              showfig=True, lbd=False, density=None, frc=False, frc_t=0):
    """
    2019-06-09

    Sample points given sampling parameters and model parameters

    Arguments:
    pm_sample= object of sampling distribution parameters
    pm_model = object of model parameters
    n = number of points in the highest selection fraction field
    fieldstuff = list of [radius, lfield, bfield, selfrac, dmin, dmax, area, locids]

    Returns:
    Acceptance fraction
    """

    #-------------------------------------------------------------------------
    # proposal model (hyper)parameters
    #-------------------------------------------------------------------------
    rrad, llfield, bbfield, ssfrac, dmin, dmax, area, locids = fieldstuff
    radius_plate = np.array(rrad)
    l_p = np.array(llfield)
    b_p = np.array(bbfield)
    selfr = np.array(ssfrac)
    selfrac = selfr

    #==========================================================================
    # draw 
    #==========================================================================

    # draw positions 
    R, z, l, b, d, loc =  draw_several_pointings_expo(n, l_p, b_p, \
                     dmin, dmax, pm_sample, radius_plate, area, selfrac,\
                     locids, dust=dust, return_maxF=False, density=density)

    nn = len(R)
    
    # draw ages
    t = age_IO(R, pm_sample, tmin=0.1, frc_t=frc_t)

    # draw velocities
    vphi, vr = draw_velocities(R, z, t, pm_sample)
    vz = np.zeros(len(vr))

    # get their actions (neglect vertical motion)
    JR, LZ,  JZ = rm.compute_actions(R, z*0, vr, vphi, vz)

    # draw their birth radii
    R0 = draw_R0(R, t, pm_sample)

    # get birth angular momentum
    vc0 = rm.calculate_vcirc(R0)
    Lz0 = R0*vc0

    #--------------------------------------------------------------------------
    # reject negative birth radii
    #--------------------------------------------------------------------------
    N_ = len(t)
    mask = (R0 > 0) & (vphi > 0)
    t = t[mask]
    vphi = vphi[mask]
    vr = vr[mask]
    vz = vz[mask]
    JR = JR[mask]
    LZ = LZ[mask]
    JZ = JZ[mask]
    vc0 = vc0[mask]
    Lz0 = Lz0[mask]
    R = R[mask]
    z = z[mask]
    l = l[mask]
    b = b[mask]
    d = d[mask]
    loc = loc[mask]
    R0 = R0[mask]

    #==========================================================================
    # compute the real model on these points
    #==========================================================================

    # birth radius distribution
    p_birth = rm.p_birth(R0, pm_model)*R0/pm_model.rd

    # age distribution
    p_age = rm.SFH_R_1(t, R0, pm_model, tmin=0.1)

    # radial migration
    p_migr = rm.p_migration_drift(LZ, t, Lz0, vc0, pm_model)

    # heating
    Rcirc = rm.R_circ(LZ)
    k = rm.epicycle_frequency(Rcirc)
    sigma_r = rm.vel_disp_r(Rcirc, t, pm_model)
    p_heating = rm.p_heating(JR, k, sigma_r)

    # vertical distribution
    p_z = rm.p_z(Rcirc, R0, t, z)

    # cast all in together
    density_model = p_birth*p_migr*p_heating*p_z*p_age

    # possbibly to select redclump stars
    if frc == True:
        f_select_RC = rm.f_RC(t)
        density_model *= f_select_RC

    #===================================================================
    # compute proposal distribution on these points
    #===================================================================

    # position distribution
    if density is None:
        p_position = rho(R, z, pm_sample)
    else:
        p_position = density(R, z, pm_sample)

    # velocity dispersion
    vel_disp = rm.vel_disp_r(R, t, pm_sample)

    # accentuate velocity dispersion to envelope the distribution
    vel_disp *= 1.1

    # vphi around the circular velocity at R
    vcirc = rm.calculate_vcirc(R)

    # vphi
    p_vphi = rm.gauss(vcirc, vphi, vel_disp)

    # vR 
    p_vR = rm.gauss(0, vr, vel_disp)

    # p_t
    p_t = p_age_IO(t, R, pm_sample, tmin=0.1, frc_t=frc_t)

    # birth radii
    sig = pm_sample.srm*np.sqrt(t/12)
    D = -pm_sample.srm**2/(2*pm_sample.rd*12)
    p_r0 = rm.gauss(R0, R + D*t, sig)

    # cast in  all together
    p_proposal = p_position*p_vphi*p_vR*p_r0*p_t

    #===============================================================================
    # take the ratio
    #===============================================================================
    ratio = density_model / p_proposal
    max_ratio = np.max(ratio)

    # keep the points with the correct probability
    p_uniform = np.random.rand(len(R))*max_ratio
    mask = p_uniform < ratio

    a = np.sum(mask)/N_


    print(pm_model.t8)
    p_model_list = [p_birth[mask], p_migr[mask], p_heating[mask], p_z[mask]]
    p_proposal_list = [p_position[mask], p_vphi[mask], p_vR[mask], p_r0[mask]]

    if not lbd:
        return [a, t[mask], R0[mask], R[mask], z[mask], Lz0[mask], LZ[mask], JR[mask],\
                                  vphi[mask], vr[mask], p_model_list, p_proposal_list]
    if lbd:
        print(d.shape)
        return [a, t[mask], R0[mask], R[mask], z[mask], Lz0[mask], LZ[mask], JR[mask],\
                                 vphi[mask], vr[mask], p_model_list, p_proposal_list,\
                                                  l[mask], b[mask], d[mask], loc[mask]]


def points_in_given_fields_DR(pm_sample, pm_model, n, fieldstuff, dust=False,
              showfig=False, lbd=False, density=None, frc=False, 
              frc_t=0, radial_migration=True):
    """
    2019-08-21 

    update from the above to incude apogee-2 fields and be more general
           use all of the usual sampling functions but with the '_DR'
           subscript

    Sample points given sampling parameters and model parameters

    Parameters
    ----------
    pm_sample:  object of sampling distribution parameters
    pm_model: object of model parameters
    n: int,
       number of points in the highest selection fraction field
    fieldstuff: list
        [radius, lfield, bfield, selfrac, dmin, dmax]
    radial_migration:boolean, Default=True
             if set: stars radial migrate
             if not, they don't. The code will bebave differently as
             rejection sampling on Dirac function is suicidal..

    Returns
    --------
    Points sampled
    list

    Notes
    -----


    History
    --------
    2019-08-21 frankel

    2021-01-29 frankel: remove the p_age as now the age sampling is exact
                        (.IO in data_kinematics_betaversion)
                        subscirpt '_old' = other version
    2020-  update from the above to incude apogee-2 fields and be more general
           use all of the usual sampling functions but with the '_DR'
           subscript

    2021-08 frankel: document
    """

    #-------------------------------------------------------------------------
    # proposal model (hyper)parameters
    #-------------------------------------------------------------------------
    rrad, llfield, bbfield, ssfrac, d_array, frac_dust, area, locids = fieldstuff
    radius_plate = np.array(rrad)
    l_p = np.array(llfield)
    b_p = np.array(bbfield)
    selfr = np.array(ssfrac)
    selfrac = selfr

    #==========================================================================
    # draw 
    #==========================================================================

    # draw positions ===
    R, z, l, b, d, loc, weight =  draw_several_pointings_expo_DR(n, l_p, b_p, \
                     d_array, frac_dust, pm_sample, radius_plate, area, selfrac,\
                     locids, dust=dust, return_maxF=False, density=density)

    nn = len(R)

    # draw ages
    t = age_IO(R, pm_sample, tmin=0.1, frc_t=frc_t)

    # draw velocities
    vphi, vr = draw_velocities(R, z, t, pm_sample)
    vz = np.zeros(len(vr))

    # get their actions
    JR, LZ,  JZ = rm.compute_actions(R, z*0, vr, vphi, vz)

    # draw their birth radii
    R0 = draw_R0(R, t, pm_sample)

    # get birth angular momentum
    vc0 = rm.calculate_vcirc(R0)
    if radial_migration:
        Lz0 = R0*vc0
    else: 
        Lz0 = LZ
        R0 = Lz0/vc0

    #--------------------------------------------------------------------------
    # reject negative birth radii
    #--------------------------------------------------------------------------
    N_ = len(t)
    mask = (R0 > 0) & (vphi > 0)
    t = t[mask]
    vphi = vphi[mask]
    vr = vr[mask]
    vz = vz[mask]
    JR = JR[mask]
    LZ = LZ[mask]
    JZ = JZ[mask]
    vc0 = vc0[mask]
    Lz0 = Lz0[mask]
    R = R[mask]
    z = z[mask]
    l = l[mask]
    b = b[mask]
    d = d[mask]
    loc = loc[mask]
    weight = weight[mask]
    R0 = R0[mask]

    #==========================================================================
    # compute the real model on these points
    #==========================================================================

    # birth radius distribution
    p_birth = rm.p_birth(R0, pm_model)*R0/pm_model.rd

    # radial migration
    if radial_migration:
        p_migr = rm.p_migration_drift(LZ, t, Lz0, vc0, pm_model)
    else: p_migr = 1 + 0*p_birth

    # heating
    Rcirc = rm.R_circ(LZ)
    k = rm.epicycle_frequency(Rcirc)
    sigma_r = rm.vel_disp_r(Rcirc, t, pm_model)
    p_heating = rm.p_heating(JR, k, sigma_r)

    # vertical distribution
    p_z = rm.p_z(Rcirc, R0, t, z)

    # age
    p_age = rm.p_age_IO(t, R0, pm_model, tmin=0.1, frc_t=frc_t)

    # cast all in together
    density_model = p_birth*p_migr*p_heating*p_z*p_age

    # possbibly select redclump stars
    if frc == True:
        f_select_RC = rm.f_RC(t)
        density_model *= f_select_RC

    #===================================================================
    # compute proposal distribution on these points
    #===================================================================

    # position distribution
    if density is None:
        p_position = rho(R, z, pm_sample)
    else:
        p_position = density(R, z, pm_sample)

    # velocity dispersion
    vel_disp = rm.vel_disp_r(R, t, pm_sample)

    # accentuate velocity dispersion to envelope the distribution
    vel_disp *= 1.1

    # vphi around the circular velocity at R
    vcirc = rm.calculate_vcirc(R)

    # vphi
    p_vphi = rm.gauss(vcirc, vphi, vel_disp)

    # vR 
    p_vR = rm.gauss(0, vr, vel_disp)

    # p_t
    p_t = p_age_IO(t, R, pm_sample, tmin=0.1, frc_t=frc_t)

    # birth radii
    if radial_migration:
        sig = pm_sample.srm*np.sqrt(t/12)
        D = -pm_sample.srm**2/(2*pm_sample.rd*12)
        p_r0 = rm.gauss(R0, R + D*t, sig)
    else:
        p_r0 = 1+ 0*p_vR

    # cast in  all together
    p_proposal = p_position*p_vphi*p_vR*p_r0*p_t

    #===============================================================================
    # take the ratio
    #===============================================================================
    ratio = density_model / p_proposal
    max_ratio = np.max(ratio)

    # keep the points with the correct probability
    p_uniform = np.random.rand(len(R))*max_ratio
    mask = p_uniform < ratio
    a = np.sum(mask)/N_

    p_model_list = [p_birth[mask], p_migr[mask], p_heating[mask], p_z[mask]]
    p_proposal_list = [p_position[mask], p_vphi[mask], p_vR[mask], p_r0[mask]]


    if showfig:
        mask_high = ratio > max_ratio / 10
        mask_low = ratio < max_ratio / 1000000
        print('N highghest:', np.sum(mask_high), np.sum(mask_low))

        plt.figure()
        plt.scatter(LZ[mask_high], np.sqrt(JR[mask_high]), s=50, c=density_model[mask_high]/np.mean(density_model), alpha=1)
        plt.scatter(LZ[mask_low], np.sqrt(JR[mask_low]), s=50, c=density_model[mask_low]/np.mean(density_model), alpha=1)

        plt.colorbar(label='density model')
        plt.xlabel('Lz')
        plt.ylabel('Jr')

        plt.figure()
        plt.scatter(LZ[mask_high], np.sqrt(JR[mask_high]), s=50, c=p_proposal[mask_high]/np.mean(p_proposal), alpha=1)
        plt.colorbar(label='proposal')
        plt.xlabel('Lz')
        plt.ylabel('Jr')


        plt.figure()
        plt.scatter(LZ[mask_high], np.sqrt(JR[mask_high]), s=50, c=ratio[mask_high]/max_ratio, alpha=1)
        plt.scatter(LZ[mask_low], np.sqrt(JR[mask_low]), s=50, c=ratio[mask_low]/max_ratio, alpha=1)
        plt.colorbar(label='ratio')
        plt.xlabel('Lz')
        plt.ylabel('Jr')

        plt.figure()
        plt.scatter(R0[mask_high], t[mask_high], s=50, c=ratio[mask_high]/max_ratio, alpha=1)
        plt.scatter(R0[mask_low], t[mask_low], s=50, c=ratio[mask_low]/max_ratio, alpha=1)
        plt.colorbar(label='ratio')
        plt.xlabel('R0')
        plt.ylabel('t')

        plt.figure()
        plt.scatter(R[mask_high], np.sqrt(z[mask_high]), s=50, c=ratio[mask_high]/max_ratio, alpha=1)
        plt.scatter(R[mask_low], np.sqrt(z[mask_low]), s=50, c=ratio[mask_low]/max_ratio, alpha=1)
        plt.colorbar(label='ratio')
        plt.xlabel('R')
        plt.ylabel('z')

        plt.figure()
        plt.scatter(t[mask_high], np.sqrt(vr[mask_high]), s=50, c=ratio[mask_high]/max_ratio, alpha=1)
        plt.scatter(t[mask_low], np.sqrt(vr[mask_low]), s=50, c=ratio[mask_low]/max_ratio, alpha=1)
        plt.colorbar(label='ratio')
        plt.xlabel('t')
        plt.ylabel('vR')

        plt.figure()
        plt.hist(R, 200, density=1, alpha=0.3)
        plt.hist(R[mask_high], density=1, alpha=0.3, label='high')
        plt.hist(R[mask_low], density=1, alpha=0.3, label='low')
        plt.hist(R[mask], density=1, histtype='step', color='k', lw=2, bins=100)
        plt.xlabel('R')
        plt.legend()

        plt.figure()
        plt.hist(t, 200, density=1)
        plt.hist(t[mask_high], density=1, alpha=0.3, label='high')
        plt.hist(t[mask_low], density=1, alpha=0.3, label='low')
        plt.hist(t[mask], density=1, histtype='step', color='k', lw=2, bins=100)
        plt.xlabel('t')
        plt.legend()

        plt.figure()
        plt.hist(R0, 200, density=1, alpha=0.3)
        plt.hist(R0[mask_high], density=1, alpha=0.3, label='high')
        plt.hist(R0[mask_low], density=1, alpha=0.3, label='low')
        plt.hist(R0[mask], density=1, histtype='step', color='k', lw=2, bins=100)
        plt.xlabel('R0')
        plt.legend()

        plt.figure()
        plt.hist(LZ, 200, density=1, alpha=0.3)
        plt.hist(LZ[mask_high], density=1, alpha=0.3, label='high')
        plt.hist(LZ[mask_low], density=1, alpha=0.3, label='low')
        plt.hist(LZ[mask], density=1,  histtype='step', color='k', lw=2, bins=100)
        plt.xlabel('Lz')
        plt.legend()

        plt.figure()
        plt.hist(JR, 200, density=1, alpha=0.3)
        plt.hist(JR[mask_high], density=1, alpha=0.3, label='high')
        plt.hist(JR[mask_low], density=1, alpha=0.3, label='low')
        plt.legend()
        plt.xlabel('JR')

    if not lbd:
        return [a, t[mask], R0[mask], R[mask], z[mask], Lz0[mask], LZ[mask], JR[mask],\
                                  vphi[mask], vr[mask], p_model_list, p_proposal_list]
    if lbd:
        print(d.shape)
        return [a, t[mask], R0[mask], R[mask], z[mask], Lz0[mask], LZ[mask], JR[mask],\
                                 vphi[mask], vr[mask], p_model_list, p_proposal_list,\
                                    l[mask], b[mask], d[mask], loc[mask], weight[mask]]



























