Installation instructions
=========================

Dependencies
------------

``MilpyWay`` requires the use of `numpy <https://numpy.org/>`__.

Installation
------------

``MilpyWay`` is currently not yet available on PyPI, but it can be
installed by downloading the source code or cloning the Gilab

       python setup.py install

command or its usual variants (``python setup.py install --user``,
``python setup.py install --prefix=/PATH/TO/INSTALL/DIRECTORY``,
etc.).

For more info, please open an Issue on the Gitlab page.
~                                                             
