API reference
=============


``MilpyWay``
------------
.. automodule:: MilpyWay.rm_new
.. automodule:: MilpyWay.fit_model


``MilpyWay.rm_new``
--------------------
.. autofunction:: rm_new.SFH_R_1
.. autofunction:: rm_new.p_birth_radius
.. autofunction:: rm_new.p_birth
.. autoclass:: rm_new.element
   :members:
.. autofunction:: rm_new.p_migration_drift
.. autofunction:: rm_new.p_migration_radius
.. autofunction:: rm_new.vel_disp_r
.. autofunction:: rm_new.p_heating
.. autofunction:: rm_new.p_z

``MilpyWay.fit_model``
----------------------
.. autofunction:: fit_model.global_model_R0
.. autofunction:: fit_model.global_model
.. autofunction:: fit_model.data_model_fe_obs
.. autofunction:: fit_model.likelihood_obs
.. autofunction:: fit_model.minus_likelihood_obs

``MilpyWay.sampler``
----------------------
.. autofunction:: sampler.points_in_given_fields_DR



