Introduction
============

``MilpyWay`` is a simplified radial migration model for the Milky Way's disk

It contains a few modules:

* ``MilpyWay.rm_new`` contains most of the Milky Way model: star formation history, chemical enrichment, distribution functions
* ``MilpyWay.fit_model`` contains most of the fitting functions and a noise model for data uncertainties
*  soon there will be a sampler to produce a mock Milky Way disk and mock datasets

