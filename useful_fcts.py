"""
Small package containing useful functions to work with:
    -- coordinates conversions
    -- selection function related functions
"""

# store uselful functions inhere
# 17/07/2018
# 01/01/2019 add m_wash in load data
import numpy as np
from astropy.coordinates import SkyCoord
from astropy import units as u
from astropy.table import Table, join

# useful constants
X_sun = 8.2           # [kpc] Gravity collaboration (2019)
z_sun = 0.0208        # [kpc] Bennett & Bovy (2019)

# absolute magnitude RC Hbanbd
M_H = -1.49


#######################################################################################
##################       CONVERSIONS                                  #################
#######################################################################################


#----------------------------------------------------------
# Galactocentric radius in function of l, b, d
#----------------------------------------------------------
def R_lbd(l, b, d):
        """
        converts galalactic coordinates to galactocentric radius

        arguments
        l = galactic longitude (deg)
        b = galactic latitude (deg)
        d = distance to the sun (kpc)

        retuns
        R = galactocentric radius (kpc)
            same type and size as l, b or d
        """
        l_rad = np.radians(l)
        b_rad = np.radians(b)
        R = np.sqrt(X_sun*X_sun \
                - 2*np.cos(l_rad)*np.cos(b_rad)*d*X_sun \
                + d*d*np.cos(b_rad)*np.cos(b_rad))
        return R

def z_bd(b, d):
        """
        converts galactic coordinates to height above
        the galactic plane

        arguments
        b = galactic latitude (deg)
        d = distance to sun (kpc)

        returns 
        z =  height above the plane (kpc)
             same type and size as b, d
        """
        b_rad = np.radians(b)
        z = np.sin(b_rad)*d + z_sun
        return z

#----------------------------------------------------------
# coordinate transformations
#----------------------------------------------------------
def calculate_distance(H):
        """
        calculates the distance to a RC star of
        magnitude H

        arguments:
        H = magnitude in H band (mag)
            if want to consider reddening, feed with
            H - AH instead, which would be the magnitude
            observed in the absence of extinction

        returns
        distance to the Sun (kpc)
        """
        # calculate distance in kpc
        return 10*10**((H-M_H)/5.)/1000.

def convert_radec_lb(ra, dec):
        """
        converts ICRS coordinates to galactic coordinates

        arguments
        ra = right ascencion 
        dec = declination

        returns 
        l = galactic longitude (deg)
        b = galactic latitude (deg)
        """
        c_icrs = SkyCoord(ra=ra*u.degree, dec=dec*u.degree, frame='icrs')
        l = c_icrs.galactic.l
        b = c_icrs.galactic.b
        return [l.value, b.value]

def convert_lbd_xyz(l, b, d):
        """
        converts galactic coordinates to xyz coordinates

        arguments
        l = galactic longitude (deg)
        b = galactic latitude (deg)
        d = distance from the sun (kpc)

        returns
        x = coordinate on axis going towards the GC
        y = coordinate on axis orthogonal to x
        z = height above the sun
        """
        x = d*np.cos(np.radians(l))*np.cos(np.radians(b))
        y = d*np.sin(np.radians(l))*np.cos(np.radians(b))
        z = d*np.sin(np.radians(b))
        return [x, y, z]

def convert_xyz_xyzgal(x, y, z, zsun=0.0208, D_GC=8.2):
    """
    convert xyz_sun coordinates to xyz_gal coordinates

    arguments
    xyz
    zsun = height of the sun above the Galactic Plane
    D_GC = distance to the galactic center

    returns
    xgal, ygal, zgal

    2019-06-03
    """
    Rsun = np.sqrt(D_GC**2 - zsun**2)
    costheta = Rsun/D_GC
    sintheta = zsun/D_GC
    x1 = -(x*costheta + z*sintheta) + Rsun + 0
    y1 = y + 0
    z1 = z*costheta - x*sintheta + zsun + 0
    return [x1, y1, z1]

def convert_xyzgal_xyz(x1, y1, z1, zsun=0.0208, D_GC=8.2):
    """
    convert xyz_gal coordinates to xyz_sun coordinates
                                   (centered on Sun)

    arguments
    xyz
    zsun = height of the sun above the Galactic Plane
    D_GC = distance to the galactic center

    returns
    xgal, ygal, zgal

    2020-09-09

    NOT TESTED NEVER TRIED
    """
    Rsun = np.sqrt(D_GC**2 - zsun**2)
    costheta = Rsun/D_GC
    sintheta = zsun/D_GC
    
    y = y1
    x = 1/costheta*(-x1 - sintheta/(costheta + sintheta**2/costheta)\
                          *(z1 - sintheta/costheta*(x1 + Rsun) + zsun) + Rsun)
    z = (z1 - sintheta/costheta*x1 + sintheta/costheta*Rsun + zsun)\
            /(costheta + sintheta**2/costheta)

    return [x, y, z]



def convert_xyzgal_R(x, y):
    """
    convert xyz gal to galactocentric radius
    2019-06-03
    """
    R = np.sqrt(x**2 + y**2)
    return R

def convert_lbd_Rgalz(l, b, D):
    xs, ys, zs = convert_lbd_xyz(l, b, D)
    xg, yg, zg = convert_xyz_xyzgal(xs, ys, zs)
    R1 = convert_xyzgal_R(xg, yg)+0
    return [R1, zg]

def convert_xyz_Rphiz(x, y, z):
        """
        converts xyz coordinates to galactocnetric

        arguments
        x, y, z (compared to sun)

        returns
        R = galactocentric radius
        phi = azimuth
        z_gal = height above the plane
        """
        x_gal = X_sun - x
        y_gal = y
        z_gal = z + z_sun
        R = np.sqrt((x_gal)*(x_gal) + y_gal*y_gal)
        phi = np.arctan(y_gal/(x_gal))
        return [R, phi, z_gal]

def convert_xyz_lbd(x, y, z):
    """ 
    convert yxz coordinates to galactic coordinates

    arguments: x, y, z

    returns: l, b, d (deg, deg, kpc)
    """

    D = np.sqrt(x*x + y*y + z*z)
    b = np.arcsin(z/D)
    l = np.arctan2(y, x)
    [l, b] = np.degrees([l, b])
    l = l + 180 #2*np.pi + l
    return l, b, D

def test_convert_xyz_lbd(l, b, d):
    """ 
    test to make sure that different conversions
    retrurn the same things

    arguments: l, b, d

    returns nothing
    prints new l, b, d that went through conversons
    """

    [x, y, z] = convert_lbd_xyz(l, b, d)
    l1, b1, d1 = convert_xyz_lbd(x, y, z)
    print('l \t b \t D \t')
    print(l,'\t', b, '\t', d)
    print(l1,'\t', b1, '\t', d1)


#----------------------------------------------------------
# Distance limits depending on zcut
#----------------------------------------------------------
def distance_limits(b, dmin, dmax, zct):
        """
        calculates new distance limits given previous
        distance limits and an additional zcut

        arguments
        b = galactic latitude (deg) array
        dmin, dmax = min and max distances arrays
        zct = z cut to add float

        returns:
        dmin = min(dmin, dmin(zut, b)) array
        dmax = min(dmax, dmax(zcut, b)) array
        """ 
        # z limints as stated if there were no z cut
        z_min = dmin*np.sin(np.radians(b))
        z_max = dmax*np.sin(np.radians(b))

        # mask those which exceed the z values we allow
        mask_min = (np.abs(z_min) > zct)
        mask_max = (np.abs(z_max) > zct)

        # replace them by values wanted
        dmin[mask_min] = zct/np.abs(np.sin(np.radians(b[mask_min])))
        dmax[mask_max] = zct/np.abs(np.sin(np.radians(b[mask_max])))

#       print('mask distance limits min ', np.sum(mask_min))
#       print('mask distance limits max', np.sum(mask_max))
#       mask_min = dmin > 

        return dmin, dmax

#---------------------------------------------------------
# additinal distance limit to the sun
#---------------------------------------------------------
def dist_limits_adhoc(dmin, dmax, d_limit):
        """
        change an array of distance limits to a new
        one with a new distance limit condition

        arguments
        dmin, dmax = arrays of distance limits
        d_limit = new distance cut

        returns
        dmin = min(dmin, d_limit)
        dmax = max(dmax, d_limit)
        as arrays (uses masks!)
        """
        mask = dmax > d_limit
        dmax[mask] = d_limit
        mask = dmin > d_limit
        dmin[mask] = d_limit
        return dmin, dmax


#############################################################################################
############   selection function related functions (taking in location id)  ################
#############################################################################################


#---------------------------------------------------------
# take data in specific fields (& incl. sel frac)
#---------------------------------------------------------
def load_lb_coord_field(l_id):
    """ 
    loads l and b of the field of location id l_id
    from the selection function file
    fields_selection_fraction_ak_dec16.fits

    argument: l_id = locatoin id of the field

    returns
    l = galacitc longitude (deg)
    b = galactic latitude (deg)
    """
    sf = Table.read('fields_selection_fraction_ak_dec16.fits')
    m = sf['LOCATION_ID'] == l_id
    ra = sf['RA'][m][0]
    dec = sf['DEC'][m][0]
    l, b = convert_radec_lb(ra, dec)
    return l, b

def load_ra_dec_coord_field(l_id):
    """
    loads ICRS coordinates of the fied of location id l_id
    from the selection function file
    fields_selection_fraction_ak_dec16.fits

    argument: l_id

    returns
    ra, dec (deg)
    """
    sf = Table.read('fields_selection_fraction_ak_dec16.fits')
    m = sf['LOCATION_ID'] == l_id
    ra = sf['RA'][m][0]
    dec = sf['DEC'][m][0]
    return ra, dec

def load_name_field(l_id):
    """
    loads name of the fied of location id l_id
    from the selection function file
    fields_selection_fraction_ak_dec16.fits

    argument: l_id

    returns
    name (string)
    """
    sf = Table.read('fields_selection_fraction_ak_dec16.fits')
    m = sf['LOCATION_ID'] == l_id
    name = sf['FIELD'][m]
    return name

def load_name_fields(lids):
    """
    load names in an array for the fields of location id lids

    argument:
        lids = array of location ids

    returns:
        names = array of names
    """
    names = np.array([])
    for i in range(len(lids)):
        name = load_name_field(lids[i])
        if len(name) > 1:
             name = np.array([name[0]])
        else:
            name = np.array(name)
        names = np.concatenate((names, name))
    return names

def load_radius_field(l_id):
    """
    load the radius of the plate of the field of location id l_id
    from the selection function file
    fields_selection_fraction_ak_dec16.fits

    argument: l_id

    returns
    radius (deg)
    """
    sf = Table.read('fields_selection_fraction_ak_dec16.fits')
    m = sf['LOCATION_ID'] == l_id
    radius = sf['RADIUS'][m][0]
    return radius

def load_Hmin_Hmax_field(l_id):
    """
    loads the magnitude limits of the short cohort of the field
    of locatoin id l_id
    from the selection function file
    fields_selection_fraction_ak_dec16.fits

    argument: l_id

    returns
    Hmin, hmax
    """

    sf = Table.read('fields_selection_fraction_ak_dec16.fits')
    m = sf['LOCATION_ID'] == l_id
    mshort = sf['SHORT'] == 1
    m = m & mshort
    Hmin = sf['H_MIN'][m]
    Hmax = sf['H_MAX'][m]
    return Hmin, Hmax

def load_fields_lb(l_min=24, l_max=240, b_min=0, b_max=14, Ak=None):
    """ 
    load short cohort field in l and b ranges
    l_min = min longitude (deg)
    l_max = max longitude
    b_max = max latitude
    """

    sf = Table.read('fields_selection_fraction_ak_dec16.fits')

    # extract info
    ra = sf['RA']                 # [deg]
    dec = sf['DEC']               # [deg]
    loc_id = sf['LOCATION_ID']    # fiedl location
    ak = sf['AK_med']             # median extinction in K

    # change coordintates
    l_data, b_data = convert_radec_lb(ra, dec) # [deg, deg]
    mask_new = (np.abs(b_data) > b_min) & (np.abs(b_data) < b_max) \
           & (l_data > l_min) & (l_data < l_max)

    if Ak is not None:
        Ak_min, Ak_max = Ak
        m_ak = (ak > Ak_min) & (ak < Ak_max)
        mask_new = mask_new & m_ak

    return np.unique(loc_id[mask_new])

def N_phot_N_spec(lid, short=True):
    """
    load short cohort Nphot and Nspec
    """
    sf = Table.read('fields_selection_fraction_ak_dec16.fits')
    m = sf['LOCATION_ID'] == lid
    N_phot = sf['N_PHOT'][m][sf['SHORT']==1]
    N_spec = sf['N_SPEC'][m][sf['SHORT']==1]
    return N_phot, N_spec

def N_phot_N_spec_fields(lids, short=True):
    """
    load short cohort Nphot and Nspec
    for several location ids
    """
    Nphot = np.zeros(len(lids))
    Nspec = np.zeros(len(lids))
    for i in range(len(lids)):
        Nphot[i], Nspec[i] = N_phot_N_spec(lids[i])
    return Nphot, Nspec

def fields_l_b_ak(l_min=24, l_max=240, b_min=0, 
                b_max=14, ak_min=0, ak_max=5.):
    """
    load short cohort fields in l, b and median ak ranges

    arguments:
    l_min = minimum field longitude (deg)
    l_max = maximum field longitude (deg)
    b_min = minimum field latitude (deg)
    b_max = maximum field latitude (deg)
    ak_min = minimum median extinction in k band
    ak_max = maximum median extinction in k band

    returns
    an array of location ids that correspond to 
    these criteria

    use the file
    fields_selection_fraction_ak_dec16.fits
    """
    sf = Table.read('fields_selection_fraction_ak_dec16.fits')

    # extract info
    ra = sf['RA']                 # [deg]
    dec = sf['DEC']               # [deg]
    loc_id = sf['LOCATION_ID']    # field location
    ak = sf['AK_med']

    # change the location ids to a numpy array of integers
    loc_id = np.array(loc_id, dtype='int')

    # change coordintates
    l_data, b_data = convert_radec_lb(ra, dec) # [deg, deg]

    # select on lb
    mask_lb = (np.abs(b_data) < b_max) \
           & (np.abs(b_data) > b_min) \
           & (l_data > l_min) & (l_data < l_max)

    # select on ak
    mask_ak = (ak > ak_min) & (ak < ak_max) & (sf['SHORT']==1)

    m = mask_lb & mask_ak
    return np.unique(loc_id[m])


def load_info_fields(l_id, short=True, med=False, Long=False, map_version=17):
    """
    loads important information for selection function (short cohort)

    arguments
    l_id = list of location ids we want info about
    short = True so far the only cohort working well

    returns
    l_data = longitudes (deg)
    b_data = latitudes (deg)
    frac = list of selection fractions
    ak_med = list of median extinctions in ak
    t_i = list of tables containing distance and frac area of visible RC stars
    """
    # load selection function information
    sf = Table.read('fields_selection_fraction_ak_dec16.fits')

    # mask location ids and short
    mask_id = np.isin(sf['LOCATION_ID'].data, np.array(l_id))

    # mask short cohort
    mask_short = (sf['SHORT'] == 1)

    # total mask
    m = mask_id & mask_short

    # extract info
    ra = sf['RA'][m].data                 # [deg]
    dec = sf['DEC'][m].data               # [deg]
    loc_id = sf['LOCATION_ID'][m].data    # field location
    ak_median = sf['AK_med'][m].data
    frac = sf['SEL_FRAC'][m].data
    hmin = sf['H_MIN'][m].data
    hmax = sf['H_MAX'][m].data
    radius = sf['RADIUS'][m].data

    # change coordintates
    l_data, b_data = convert_radec_lb(ra, dec) # [deg, deg]

    # load the fractional areas with the dust for these fields
    if map_version == 17:
        folder_name='fractions_area_allfields/'
    elif map_version == 19:
        folder_name='fractions_area_allfields_2019/'
    t_i =[]
    for i in range(len(l_id)):
       d, area = np.load(folder_name\
                           +str(int(l_id[i]))+'_indebetouw.npy')
       t_i.append(Table([d, area], names=('DISTANCE', 'FRAC_AREA'), meta={'name': str(l_id[i])}))       
    return l_data, b_data, frac, ak_median, hmin, hmax, radius, t_i


class selection_function_for_integral(object):
    """
    selection function class that contains the information we need
    to integrate the model over the survey volume.
    Not optimum coding (should male the 'load_info_fields' a function
    inside of this class...
    """
    def __init__(self, lid, map_version=17):
        self.loc_id = lid
        self.l, self.b, self.frac, self.akmed, \
               self.hmin, self.hmax, self.radius, self.effective = \
               load_info_fields(lid, short=True, med=False, Long=False, \
               map_version=map_version)
        self.dmin = calculate_distance(self.hmin)
        self.dmax = calculate_distance(self.hmax)
        self.area = (1 - np.cos(np.radians(self.radius)))*2*np.pi


def load_data_field(l_id, dataset_path, short=True, 
       med=False, Long=False,age_min=0, age_max=14, 
               feh_min=-3, feh_max=3, xmatch=False, 
              wash=False, data=None, selfunc=None,
              path_to_yst='../data/Catalog_Apogee_RC.fits'):
    """
    loads the RC data contained in a single field of location id l_id

    arguments
    l_id = location id of the field to load
    data_set_path = path to the dataset to the rc data
    so far only works for short cohort!!! set short=True
    med=False
    long=False
    age_min=0 = age min if want to select age
    age_max=14 = max age if want to select by age
    feg_min, feh_max = possible metallicity selection

    returns a list of things
           loc_id, h_min, h_max, Extinc_med, 
           sel_frac, radius, lfield, bfield, 
           l_d, b_d, D_d, Fe, age
    """
    yst = Table.read(path_to_yst)

    if data == None:
        data = Table.read(dataset_path)

    if xmatch:
        data = join(data, yst, keys='APOGEE_ID', join_type='inner')

    if selfunc == None:
        sf = Table.read('fields_selection_fraction_ak_dec16.fits')
    else:
        sf = selfunc

    # mask the selection fractions
    m_sf = sf['LOCATION_ID'] == l_id
    m_short = sf['SHORT']==1
    m = m_sf & m_short 
    sf = sf[m]

    # cut the stars based on distances (cohorts)
    dmin = calculate_distance(sf['H_MIN'])# - 1.55*sf['AK_med'])
    dmax = calculate_distance(sf['H_MAX'])#- 1.55*sf['AK_med'])
 
#    age_yst = 10**data['Log_Age_Myr']/1000
    # mask the data set
    m_did = data['LOCATION_ID'] == l_id
    m_dshort = data['APOGEE_TARGET1'] & 2**11 != 0
    m_age = (data['age'] > age_min) & (data['age'] < age_max)
#    m_age = np.isfinite(data['Log_Age_Myr']) & (age_yst > age_min) & (age_yst < age_max)

    m_fe = (data['Feh'] > feh_min) & (data['Feh'] < feh_max)
    m_h = (data['H'] > sf['H_MIN']) & (data['H'] < sf['H_MAX'])
    m_dist = (data['RC_DIST'] > dmin-2) & (data['RC_DIST'] < dmax+4) \
             & np.isfinite(data['RC_DIST'])

    # mask the low alpha disk
    a = -0.12# -0.15
    b = 0.14 # 0.11 
    # mask above/below the cut
    m_low = (data['alphafe'] < a*data['Feh'] + b)

    m_d = m_did & m_dshort & m_age & m_dist & m_fe & m_h & m_low 

    if wash:
        m_wash = (data['APOGEE_TARGET1'] & 2**7 == 0)\
               & (data['APOGEE_TARGET1'] & 2**6 == 0)\
               & (data['APOGEE_TARGET1'] & 2**9 == 0)
        m_d = m_d & m_wash

    data = data[m_d]

    h_min = sf['H_MIN']                      # [mag] h band
    h_max = sf['H_MAX']                      # [mag] h band
    Extinc_med = sf['AK_med']                # median extinction apogee *
    sel_frac = sf['SEL_FRAC']                # selection fraction
    radius = sf['RADIUS']                    # opening angle pointings [deg]
    loc_id = int(sf['LOCATION_ID'].data)     # fiedl location
    lfield, bfield = convert_radec_lb(sf['RA'], sf['DEC'])      # [deg, deg]
    
    l_d = data['GLON']
    b_d = data['GLAT']
    D_d = data['RC_DIST']
    Fe = data['Feh']
    Mg = data['MG_H']
    O = data['O_H']
    age = data['age']

#    age = age_yst[m_d]

    if not xmatch:
        return loc_id, h_min, h_max, Extinc_med, \
               sel_frac, radius, lfield, bfield, \
               l_d, b_d, D_d, Fe, age#, data['RC_GALR'], data['RC_GALZ']#, t

    if xmatch:
        print('xmatch')
        vphi = data['Spec_Galactic_Velocity_Phi']
        vr = data['Spec_Galactic_Velocity_R']
        Lz = data['Spec_Action_Phi']
        JR =data['Spec_Action_R']
        age = 10**data['Log_Age_Myr']/1000

        return loc_id, h_min, h_max, Extinc_med, \
               sel_frac, radius, lfield, bfield, \
               l_d, b_d, D_d, Fe, age, data['RC_GALR'], data['RC_GALZ'],\
               Lz, JR, vphi, vr



def load_data_fields(l_ids, dataset_path, short=True, med=False, Long=False,
    age_min=0, age_max=14, feh_min=-3, feh_max=3, xmatch=False, wash=False):
    """
    loads the RC data contained in a several fields of location id l_ids

    arguments
    l_ids = location ids of the fields to load
    data_set_path = path to the dataset to the rc data
    so far only works for short cohort!!! set short=True
    med=False
    long=False
    age_min=0 = age min if want to select age
    age_max=14 = max age if want to select by age
    feg_min, feh_max = possible metallicity selection

    returns a list of things
           loc_id, h_min, h_max, Extinc_med, 
           sel_frac, radius, lfield, bfield, 
           l_d, b_d, D_d, Fe, age

    each of these things is a list of length the number of fields
    those containing the stars properties are arrays, of lengths the
          number of stars inside each field

    """
    # preload selection function
    sf = Table.read('fields_selection_fraction_ak_dec16.fits')


    if isinstance(l_ids, float) or isinstance(l_ids, int):
        data = load_data_field(l_ids, dataset_path, short=True, 
               med=False, Long=False, xmatch=xmatch, wash=wash)

    else:
        # preload the data 
        pre_data = Table.read(dataset_path)

        # define the lists of info to return
        loc_id = []
        h_min = []
        h_max= []
        Extinct_med = []
        sel_frac= []
        radius = []
        lfield = []
        bfield = []
        l_d = []
        b_d = []
        D_d = []
        Fe = []
        age = []

        if xmatch:
            R = []
            z = []
            Lz = []
            JR = []
            vphi = []
            vr = []

        N_stars = 0
        # loop over the location ids provided
        for i in range(len(l_ids)):
#            print(i)
            l_id = l_ids[i]
            # load the data here
            d = None
            d = load_data_field(l_id, dataset_path, short=True, med=False, Long=False, 
                   age_min=age_min, age_max=age_max, feh_min=feh_min, feh_max=feh_max, 
                                xmatch=xmatch, wash=wash, data=pre_data, selfunc = sf)
            N_stars += len(d[12])
            if (len(d[12]) < 1): #or (d[4] <= 0):
                print('empty field, ',i, l_ids[i])

            # returns the data only if the selection fraction is non zero in that field: d[4] > 0
            if (d[4] > 0):# & (len(d[12]) > 1) :
               print(i)
               loc_id.append(d[0])
               h_min.append(d[1])
               h_max.append(d[2])
               Extinct_med.append(d[3])
               sel_frac.append(d[4])
               radius.append(d[5])
               lfield.append(d[6])
               bfield.append(d[7])
               l_d.append(d[8])
               b_d.append(d[9])
               D_d.append(d[10].data)
               Fe.append(d[11])
               age.append(d[12])

               if xmatch:
                   R.append(d[13])
                   z.append(d[14])
                   Lz.append(d[15])
                   JR.append(d[16])
                   vphi.append(d[17])
                   vr.append(d[18])

        data = loc_id, h_min, h_max, Extinct_med, \
              sel_frac, radius, lfield, bfield, \
              l_d, b_d, D_d, Fe, age

        if xmatch:
            data = loc_id, h_min, h_max, Extinct_med, \
                  sel_frac, radius, lfield, bfield, \
                  l_d, b_d, D_d, Fe, age, R, z, \
                  Lz, JR, vphi, vr

        print('useful: N_stars = ', N_stars)
    return data


def interpolate_effective_selfunc(eff_frac, mag):
    """
    2019-08-21
    Interpolate the effective selection fraction as given in 
    the selection function table, for a standard candle of 
    absolute magnitude mag.

    Arguments
        eff_frac: 3D array: N_fields x N_distances x N_mag
                  as given in the selection function table grids

        mag: float: absolute magnitude of the standard candle
             we want to find the selection function for (not
             hidden behind dust)

    Returns:
        eff_frac(fields, distance | mag): 2D array 
        N_fields x N_distances
        of the selection dust fractions

    """
    # find the corresponding values in magnitude
    #mag_c = np.linspace(-7, -1, 20)  # grid as I gave it in the table
    mag_c = np.linspace(-5, -1, 27)   # grid as I gave in the other table
    diff = mag_c - mag         
    m1 = diff <= 0

    # find indices enclosing the magnitude we want
    i_less = np.sum(m1) - 1 
    i_great = i_less + 1
    
    # find the weight to take the weighted mean
    fact1 = (mag_c[i_great] - mag)/(mag_c[i_great] - mag_c[i_less])
    fact2 = (mag - mag_c[i_less])/(mag_c[i_great] - mag_c[i_less])
    
    # return the effective fractions: take the weighted mean
    frac = eff_frac[:,:,i_less]*fact1 + eff_frac[:,:,i_great]*fact2
    return frac

#==================================================================
# more general way of loading data in given fields
#==================================================================
def load_table_fields(table, locids):
    """
    Load the rows of the table where the location ids match
    locids

    Arguments
        table = astropy table that contains a column 'LOCATION_ID'
        locids = a list of array of location ids we are interested in

    Returns
        a table with only these locids
    """
    m = np.zeros(len(table), dtype='bool')      # empty mask
    for i in range(len(locids)):           # loop over locids we want
        m += locids[i] == table['LOCATION_ID']  # fill the mask
    new_table = table[m]                        # create new table
    return new_table 

def apply_spatial_select(l, b, D, ls, bs, Dmins, Dmaxs, rad):
    """
    2019-12-21 Frankel

    Select subset stars with l, b, D in a field in lbDrad

    Arguments:
        l Galactic longitude of the stars to select in deg, np array
        b Galactic latitude of the stars to select in deg, np array
        D distances of the stars to select in kpc, np array
        ls Galactic lontitude of the center of the field
        bs Galactic latitude of the center of the field
        Dmins, Dmaxs distance limits in that field
        rad Radius of the field in deg

    Returns:
        a mask of the stars that belong to the field
    """
    # calculate distance between stars and field
    dist2 = (l-ls)*(l-ls) + (b-bs)*(b-bs)
    rad2 = rad*rad

    # construct the mask
    mask_cone = dist2 < rad2
    mask_dist = (D > Dmins) & (D < Dmaxs)
    mask_tot = mask_cone & mask_dist
    return mask_tot

def apply_fraction_selection(N, frac):
    """
    2019-12-21 Frankel

    Downselect stars according to some selection fraction

    Arguments:
        N: size of current sample
        frac: fraction of stars to select from that sample

    Returns: mask of size N
    """
    u = np.random.rand(N)
    take = u <= frac
    return take


def apply_apogee_spatial_selfunc(l, b, D, table_sf):
    """
    2019-12-21 Frankel

    Apply spatial selection function to a set of stars

    Arguments:
       l, b, D = glon, glat (deg), distance (kpc); np array

    Returns
       mask of stars to keep
    """
    Ntot = len(l)
    mask_keep = np.zeros(Ntot, dtype='bool')
    for i in range(len(table_sf)):
        Dmins, Dmaxs = 0, 5
        ls = table_sf['GLON'].data[i]
        bs = table_sf['GLAT'].data[i]
        rad = table_sf['RADIUS'].data[i]
        frac = table_sf['SEL_FRAC'].data[i]
        mask_field_i = apply_spatial_select(l, b, D, 
                ls, bs, Dmins, Dmaxs, rad)
        mask_fraci = apply_fraction_selection(Ntot, frac)
        mask_i = mask_field_i & mask_fraci
        mask_keep += mask_i
    return mask_keep




##########################################################################################
# statistics
##########################################################################################

def KS_test(x1, x2):
    fcumul_x1 = np.cumsum(x1)
    fcumul_x2 = np.cumsum(x2)
    
    return 1

##########################################################################################
# Optimization
##########################################################################################
def Newton_Raphson(func, x_guess, y_root, dx, ytol, xstep=0.9):
    err_abs = np.inf
    x = x_guess
    while np.any(err_abs > ytol):
        y = func(x)
        #plt.figure()
        #plt.scatter(x, y)
        #plt.show()
        dy = func(x + dx) - y
        dydx = dy/dx
        err = y - y_root
        err_abs = np.abs(err)
        dx_step = -err*dx/dy
        x = x + xstep*dx_step
        x[x <0] = .1
        print(np.sum(np.isfinite(y)), np.sum(err_abs/len(err)), np.max(err_abs))
    return x - dx_step


########################################################################################################################
########################################################################################################################
if __name__ == '__main__':
    l_id = 4591
    dataset_path = 'apogee-rc-DR12.fits'
    loc_id, h_min, h_max, Extinct_med, sel_frac,\
            radius, lfield, bfield, l_d, b_d, D_d, \
            Fe, R, z = load_data_field(l_id, \
            dataset_path, short=True, med=False, long=False)

    print(loc_id)
    print(h_min)
    print(h_max)
    print(sel_frac)
    print(len(Fe))
    import matplotlib.pyplot as plt
    plt.scatter(l_d, b_d)

    Rstars = R_lbd(l_d, b_d, D_d)
    zstars = z_bd(b_d, D_d)

    plt.figure()
    plt.scatter(R, Rstars)
    plt.figure()
    plt.scatter(z, zstars)
    plt.show()
































