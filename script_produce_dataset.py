# 2019-06-07
# 2019-06-23 For real APOGEE selection function

#============================================================================
# Import modules
#============================================================================
import numpy as np
import matplotlib.pyplot as plt
import rm_new as rm
import useful_fcts as uf
import time
import sampler as sample
#from load_my_fields import *
from multiprocessing import Pool
from astropy.table import Table

# Make enrichment
element = rm.element()

#============================================================================
# sampling parameters
#============================================================================
# submitted version:
#pm_sample = sample.sampling_parameters(srm=5.3, rd=4, x=0.7, tsfr=3, t8=11,
#                            sigma_vr0=53, Rd_heating=18, c4=1, beta=0.3)

# No migration for Blanton:
pm_sample = sample.sampling_parameters(srm=5.3, rd=4, x=0.7, tsfr=3, t8=11,
                            sigma_vr0=53, Rd_heating=18, c4=1, beta=0.3)

# Submitt version
#pm_model = rm.all_parameters(srm=823, t_old=6, t_T=6, tsfr=1., sigma_vr0=49.3,  
#                             Rd_heating=19, c4=1, rd=2.84, x=0.65, beta=0.30,
#                             a=1.16)

pm_model = rm.all_parameters(srm=823, t_old=6, t_T=6, tsfr=1., sigma_vr0=47,
                             Rd_heating=27, c4=1, rd=2.85, x=0.67, beta=0.30,
                             a=1.16)
pm_model.t8=6

#============================================================================
# field coordinates
#============================================================================

# ------------------- Load the APOGEE selection function --------------------
# load apogee selection function
sf = Table.read('sf_apogee_dr14_disk.fits')

mask_short = ((sf['COHORT'] == 's') + (sf['SURVEY'] =='apogee')) & (sf['H_MIN']<8)
mask_blue = (sf['JK_MIN'] < 0.6)
mask_selfrac = sf['SEL_FRAC'] > 0


# keep only the desired locids and the blue bins when apogee-2
mask = mask_short & mask_blue & mask_selfrac

sf = sf[mask]

# unpack the selection function
rrad = sf['RADIUS'].data
llfield, bbfield = sf['GLON'].data, sf['GLAT'].data
ssfrac = sf['SEL_FRAC'].data
d_array = sf['DIST'].data
frac_dust = sf['FRAC_DUST'].data
area =  1 - np.cos(np.radians(rrad))
locids = sf['LOCATION_ID']

# interpolate the effective selection function
fracdust = uf.interpolate_effective_selfunc(frac_dust, -1.49)
darray = d_array[:,:,0]

# pack together relevant information
fieldstuff = [rrad, llfield, bbfield, ssfrac, darray, fracdust, area, locids]

#============================================================================
# n is the maximum number of stars generated in single apogee field
# will be downsampled later by taking the ratio of two models
n = 10000

#============================================================================
# sample the data
#============================================================================
data = sample.points_in_given_fields_DR(pm_sample, pm_model, n, fieldstuff,
       dust=True, showfig=False, lbd=True, density=None, frc=True, frc_t=3,
       radial_migration=True)

#============================================================================
# save the data
#============================================================================

# save the data
#folder = 'produced_importance_samples_integral_apo_test/'
#folder = 'samples_test_thick_disk/'
#folder = 'Blanton/' 
#name = 'test_all_fields_33_bestfit_2019-12-26_RC1p49_onlineselfunc'
#name = '2021-01_bestfit_nomigr1'

saving = None#'fits'

if saving == 'fits':
    print('Number of stars: ', len(data[1]))
    names = ['LOCATION_ID', 'GLON', 'GLAT', 'DIST', 'R', 'Z',
             'AGE', 'V_PHI', 'V_R', 'L_Z', 'J_R', 'R0', 'L_Z0']
    a, t, R0, R, z, Lz0, Lz, Jr, vphi, vr, pmod, pprop, l, b, D, loc, weight = data
    tosave = [loc, l, b, D, R, z, t, vphi, vr, Lz, Jr, R0, Lz0]
    t = Table(data=tosave, names=names)
    comments = pm_model.__dict__
    t.meta['comments'] = comments
    t.write(folder+name+'.fits', format='fits', overwrite=True)


#============================================================================
# Plot stuff
#============================================================================
plotting = True
if plotting == True:
    locid = data[15]
    l = data[12]
    b = data[13]

    print('field 1: ', np.sum(locid == 1))
    print('field 2: ', np.sum(locid == 2))
    D = data[14]
    print(len(D))
    xx1, yy1, zz1 = uf.convert_lbd_xyz(l, b, D)
    xx, yy, zz = uf.convert_xyz_xyzgal(xx1, yy1, zz1, zsun=0.0208, D_GC=8.2)

    vphi = data[8]
    t = data[1]
    R0 = data[2]

    plt.figure()
    plt.hist(D,60)

    plt.figure()
    plt.scatter(xx, yy)

    plt.figure()
    plt.hist(vphi, 60)

    plt.figure()
    plt.hist(t, 60)

    plt.figure()
    plt.hist(R0, 60)

    plt.show()
